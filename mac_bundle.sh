#! /bin/bash

APPNAME="installer"
#if [ -z $APPNAME ]; then
#    echo Usage: $0 make-netflix
#    exit 1
#fi

rm -rf dist/

BUNDLENAME="${APPNAME}_mac"
CONTENTS="dist/${BUNDLENAME}.app/Contents"

# copy executable
mkdir -p "$CONTENTS/MacOS"
cp target/release/equoinstaller "$CONTENTS/MacOS/$APPNAME" 
strip "$CONTENTS/MacOS/$APPNAME"

# copy icon
mkdir -p "$CONTENTS/Resources"
cp icon.icns "$CONTENTS/Resources/"

# copy plist
sed s/cefrust_subp/$APPNAME/g Info.plist > "$CONTENTS/Info.plist"

cd dist
zip -r "$BUNDLENAME.zip" "$BUNDLENAME.app"