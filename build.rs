// extern crate lzma;
extern crate xz2;

// use lzma::LzmaWriter;
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::PathBuf;
// use std::io::prelude::*;
use xz2::stream::{Check, Filters, LzmaOptions, Mode, Stream};
use xz2::write::XzEncoder;

#[cfg(windows)]
extern crate winres;

#[cfg(windows)]
fn main() {
    let mut res = winres::WindowsResource::new();
    res.set_icon("make.ico");
    res.compile().unwrap();

    do_main();
}

#[cfg(unix)]
fn main() {
    do_main();
}

fn do_main() {
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    let profile = env::var("PROFILE").unwrap();

    let mut bin_f = File::create(out_path.join("binaries.rs")).unwrap();
    writeln!(
        bin_f,
        "pub fn expand(folder_path_param: &Path, handlers: &mut Vec<JoinHandle<()>>) -> PathBuf {{"
    )
    .unwrap();
    if cfg!(windows) {
        compress("cef/Release/libcef.dll", &mut bin_f, false);
        compress("cef/Release/chrome_elf.dll", &mut bin_f, false);
    } else if cfg!(target_os = "macos") {
        compress_to(
            "cef/Release/Chromium Embedded Framework.framework/Chromium Embedded Framework",
            "\"Chromium Embedded Framework.framework/Chromium Embedded Framework\"",
            &mut bin_f,
            true,
            true,
        );
    } else {
        compress("cef/Release/libcef.so", &mut bin_f, false);
    }
    if cfg!(target_os = "macos") {
        compress_to(
            "cef/Release/Chromium Embedded Framework.framework/Resources/icudtl.dat",
            "\"Chromium Embedded Framework.framework/Resources/icudtl.dat\"",
            &mut bin_f,
            false,
            true,
        );
        compress_to(
            "cef/Release/Chromium Embedded Framework.framework/Resources/cef.pak",
            "\"Chromium Embedded Framework.framework/Resources/cef.pak\"",
            &mut bin_f,
            false,
            true,
        );
    } else {
        compress("cef/Resources/icudtl.dat", &mut bin_f, false);
        compress("cef/Resources/cef.pak", &mut bin_f, false);
    }
    if cfg!(windows) {
        compress_to(
            &format!("target/{}/browser.exe", profile),
            "\"cefrust_subp.exe\"",
            &mut bin_f,
            true,
            true,
        );
        compress_to(
            &format!("target/{}/wpfApps.exe", profile),
            "\"wpfapps.exe\"",
            &mut bin_f,
            true,
            true,
        );
    } else if cfg!(target_os = "macos") {
        let browser = format!("target/{}/browser", profile);
        std::process::Command::new("install_name_tool")
            .arg("-change")
            .arg("@rpath/Frameworks/Chromium Embedded Framework.framework/Chromium Embedded Framework")  
            .arg("@loader_path/../../../Chromium Embedded Framework.framework/Chromium Embedded Framework")
            .arg(&browser)
            .output()
            .expect("failed to execute install_name_tool");
        compress_to(
            &browser,
            "\"cefrust_subp.app/Contents/MacOS/cefrust_subp\"",
            &mut bin_f,
            true,
            true,
        );
        compress_to(
            "Info.plist",
            "\"cefrust_subp.app/Contents/Info.plist\"",
            &mut bin_f,
            false,
            true,
        );
        // compress_to("PkgInfo", "\"cefrust_subp.app/Contents/PkgInfo\"", &mut bin_f, false, true);
        compress_to(
            "icon.icns",
            "\"cefrust_subp.app/Contents/Resources/icon.icns\"",
            &mut bin_f,
            false,
            true,
        );
    } else {
        compress_to(
            &format!("target/{}/browser", profile),
            "\"cefrust_subp\"",
            &mut bin_f,
            true,
            true,
        );
    }
    // compress("cefrust_subp", &mut bin_f, true); Not needed
    if cfg!(target_os = "macos") {
        compress_to(
            "cef/Release/Chromium Embedded Framework.framework/Resources/snapshot_blob.bin",
            "\"Chromium Embedded Framework.framework/Resources/snapshot_blob.bin\"",
            &mut bin_f,
            false,
            true,
        );
        compress_to(
            "cef/Release/Chromium Embedded Framework.framework/Resources/natives_blob.bin",
            "\"Chromium Embedded Framework.framework/Resources/natives_blob.bin\"",
            &mut bin_f,
            false,
            true,
        );
        compress_to(
            "cef/Release/Chromium Embedded Framework.framework/Resources/en.lproj/locale.pak",
            "\"Chromium Embedded Framework.framework/Resources/en.lproj/locale.pak\"",
            &mut bin_f,
            false,
            true,
        );
    } else {
        compress("cef/Release/snapshot_blob.bin", &mut bin_f, false);
        compress("cef/Release/natives_blob.bin", &mut bin_f, false);
        compress_to(
            "cef/Resources/locales/en-US.pak",
            "\"locales/en-US.pak\"",
            &mut bin_f,
            false,
            true,
        );
    }
    if cfg!(windows) {
        writeln!(bin_f, "folder_path_param.join(\"wpfapps.exe\")").unwrap();
    } else if cfg!(target_os = "macos") {
        // writeln!(bin_f, "folder_path_param.join(\"cefrust_subp.app/Contents/MacOS/cefrust_subp\")").unwrap();
        writeln!(bin_f, "folder_path_param.join(\"cefrust_subp.app\")").unwrap();
    } else {
        writeln!(bin_f, "folder_path_param.join(\"cefrust_subp\")").unwrap();
    }
    writeln!(bin_f, "}}").unwrap();

    writeln!(bin_f, "pub fn expand_html(folder_path_param: &Path, handlers: &mut Vec<JoinHandle<()>>) -> PathBuf {{").unwrap();
    compress("html/dist/index.html", &mut bin_f, false);
    compress("html/dist/make-m.png", &mut bin_f, false);
    writeln!(bin_f, "folder_path_param.join(\"index.html\")").unwrap();
    writeln!(bin_f, "}}").unwrap();

    let mut bin_f = File::create(out_path.join("bin_launcher.rs")).unwrap();
    writeln!(
        bin_f,
        "pub fn expand_launcher(folder_path_param: &Path, app_name: &str) -> PathBuf {{"
    )
    .unwrap();
    if cfg!(windows) {
        compress_to(
            &format!("target/{}/launcher.exe", profile),
            "format!(\"{}.exe\", app_name)",
            &mut bin_f,
            true,
            false,
        );
    } else {
        compress_to(
            &format!("target/{}/launcher", profile),
            "app_name",
            &mut bin_f,
            true,
            false,
        );
    }
    writeln!(bin_f, "   folder_path_param.join(app_name)").unwrap();
    writeln!(bin_f, "}}").unwrap();
}

fn compress(name: &str, bin_f: &mut File, executable: bool) {
    compress_to(name, "", bin_f, executable, true);
}

fn compress_to(
    name: &str,
    rel_rt_target: &str,
    bin_f: &mut File,
    executable: bool,
    threaded: bool,
) {
    println!("cargo:rerun-if-changed={}", name);
    let source_path = PathBuf::from(name);
    if !source_path.exists() {
        if executable {
            return;
        }
        panic!("{:?} does not exists", source_path);
    }
    let filename = source_path.file_name().unwrap();
    let rt_target = if rel_rt_target.is_empty() {
        format!("\"{}\"", filename.to_str().unwrap())
    } else {
        format!("{}", rel_rt_target)
    };
    let ext = match source_path.extension() {
        None => String::from("xz"),
        Some(e) => format!("{}.{}", e.to_str().unwrap(), "xz"),
    };

    let mut target_path = PathBuf::from("target").join(filename);
    target_path.set_extension(ext);

    let source = File::open(name).unwrap();
    let source_meta = source.metadata().unwrap();
    let source_len = source_meta.len();

    if !target_path.exists()
        || source_meta.modified().unwrap() > target_path.metadata().unwrap().modified().unwrap()
    {
        if cfg!(target_os = "linux")
            && source_path
                .extension()
                .unwrap_or(std::ffi::OsStr::new("so"))
                == "so"
        {
            println!("cargo:warning=stripping {:?}", name);
            std::process::Command::new("strip")
                .arg("-s")
                .arg("-v")
                .arg("-p")
                .arg(name)
                .output()
                .expect("failed to execute process");
        }
        if cfg!(target_os = "macos")
            && executable
            && source_path
                .extension()
                .unwrap_or(std::ffi::OsStr::new("dylib"))
                == "dylib"
        {
            println!("cargo:warning=stripping {:?}", name);
            std::process::Command::new("strip")
                .arg(name)
                .output()
                .expect("failed to execute process");
        }
        println!("cargo:warning=zipping {:?}", target_path);
        let target = File::create(target_path.clone()).unwrap();
        let mut source_buf = BufReader::new(source);

        let mut buf = std::vec::Vec::new();
        source_buf.read_to_end(&mut buf).unwrap();

        // TODO best compression level, but slow. Try different opts for similar size but faster decompress time
        let mut opts = LzmaOptions::new_preset(9).unwrap();
        let opts = opts.mode(Mode::Normal);
        // let opts = opts.mode(Mode::Fast);
        let mut filters = Filters::new();
        let filters = filters.x86().lzma2(opts);

        // let mut encoder = xz2::write::XzEncoder::new(target, 9);
        let mut encoder = XzEncoder::new_stream(
            target,
            Stream::new_stream_encoder(filters, Check::None).unwrap(),
        );

        encoder.write_all(buf.as_slice()).unwrap();
    } else {
        // println!("cargo:warning=skipping {:?}", target_path);
    }

    let create_parent = if !rel_rt_target.is_empty() {
        r#"
        let parent = target_path.parent().unwrap();
        if folder_path != parent && !parent.exists() {
            ::std::fs::create_dir_all(target_path.parent().unwrap()).unwrap();
        }
        "#
    } else {
        ""
    };
    let mode = if executable && cfg!(unix) {
        "if cfg!(unix) { f.mode(0o755); }"
    } else {
        ""
    };
    let logic = format!(
        r#"
        let cef_bytes = include_bytes!(r"../../../../../{}");
        let now = Instant::now();
        let target_path = folder_path.join({});
        let len = match target_path.metadata() {{
            Ok(m) => m.len(),
            Err(_) => 0
        }};
        if !target_path.exists() || {} != len {{
            {}
            let mut opts = ::std::fs::OpenOptions::new();
            let f = opts.create(true).write(true);
            {}
            let f = f.open(&target_path).expect("Can't create file");
            let mut decoder = xz2::write::XzDecoder::new(f);
            decoder.write_all(cef_bytes).unwrap();
            let elapsed = now.elapsed();
            println!("extracted {} ({{}}K) in {{:.5}}s", cef_bytes.len()/1024, elapsed.as_secs() as f64+ elapsed.subsec_nanos() as f64 * 1e-9);
        }}
        "#,
        target_path.to_str().unwrap(),
        rt_target,
        source_len,
        create_parent,
        mode,
        filename.to_str().unwrap()
    );

    if threaded {
        write!(
            bin_f,
            r#"
    let folder_path = folder_path_param.to_owned();
    handlers.push(spawn(move|| {{
        {}
    }}));
    "#,
            logic
        )
        .unwrap();
    } else {
        writeln!(
            bin_f,
            r#"
        let folder_path = folder_path_param;
        {}"#,
            logic
        )
        .unwrap();
    };
}
