## Build HTML

npm install
npm build
npm inline

## Build Native

Link or copy CEF to ./cef:  
ln -s $PWD/../../make.cefswt/cefswt/build/cef_binary_3.3071.1649.g98725e6_windows64_minimal cef

cargo build --bin launcher --release  
cargo build --bin browser --release  
cargo build --bin equoinstaller --release 

strip target/release/equoinstaller