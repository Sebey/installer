import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';
import html from 'rollup-plugin-fill-html';
// import fileAsBlob from 'rollup-plugin-file-as-blob';
// import filesize from 'rollup-plugin-filesize';
import uglify from 'rollup-plugin-uglify';
import postcss from 'rollup-plugin-postcss';
import cssnano from 'cssnano';

export default {
    input: 'index.js',
    output: {
        format: 'es',
        // format: 'iife',
        file: 'build/bundle.js'
    },
    plugins: [
        // filesize(),
        nodeResolve({
            main: true
        }),
        commonjs({
            namedExports: {
                // left-hand side can be an absolute path, a path
                // relative to the current directory, or the name
                // of a module in node_modules
                'node_modules/please-wait/build/please-wait.js': [ 'pleaseWait' ]
            }
        }),
        postcss({
            extensions: ['.css'], 
            plugins: [
                cssnano()
            ],
            extract: 'build/bundle.css',
            sourceMap: false, // defult false 
            // minimize: true
        }),
        html({
            template: 'template.html',
            filename: 'bundle.html'
        }),
        // fileAsBlob({
        //     include: '*.png',
        // }),
        uglify()
    ]
  };