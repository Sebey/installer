import anime from 'animejs'
import ProgressBar from 'progressbar.js'
// import {pleaseWait} from 'please-wait'
import { pleaseWait } from './node_modules/please-wait/build/please-wait.js'
// import logo from './make-m.png';
import './node_modules/please-wait/build/please-wait.css';
import './styles.css';

function anim() {
    var a = anime.timeline({loop: true})
    a.add({
        targets: '.ml3 .letter',
        opacity: [0,1],
        easing: "easeInOutQuad",
        duration: 200,
        delay: function(el, i) {
          return 50 * (i+1)
        }
    }).add({
        targets: '.ml3',
        opacity: 0,
        duration: 300,
        easing: "easeOutExpo",
        delay: 1000
    });

    launch_anim();
    return a;
}

function launch_anim() {
    anime.timeline({loop: true})
    .add({
      targets: '.ml5 .line',
      opacity: [0.5,1],
      scaleX: [0, 1],
      easing: "easeInOutExpo",
      duration: 700
    }).add({
      targets: '.ml5 .line',
      duration: 600,
      easing: "easeOutExpo",
      translateY: function(e, i, l) {
        var offset = -0.625 + 0.625*2*i;
        return offset + "em";
      }
    }).add({
      targets: '.ml5 .letters-left',
      opacity: [0,1],
      translateX: ["0.5em", 0],
      easing: "easeOutExpo",
      duration: 600,
      offset: '-=300'
    }).add({
      targets: '.ml5 .letters-right',
      opacity: [0,1],
      translateX: ["-0.5em", 0],
      easing: "easeOutExpo",
      duration: 600,
      offset: '-=600'
    }).add({
      targets: '.ml5',
      opacity: 0,
      duration: 1000,
      easing: "easeOutExpo",
      delay: 1000
    });
}

function progress(p, dura) {
    bar.animate(p, {
        duration: (dura || 1) * 1000
    });
}

function get_msg(m, prog) {
    function wrap_letter(txt) {
        // Wrap every letter in a span
        return txt.replace(/([^\x00-\x80]|\w|!|')/g, "<span class='letter'>$&</span>");
    }
    
    if (m.indexOf('nstalled') > -1) {
      return `<p class="loading-message ml3">${wrap_letter(m)}</p>
          <a href="javascript:launch_clicked();"><h1 class="ml5">
            <span class="text-wrapper">
              <span class="line line1"></span>
              <span class="letters letters-left">LAU</span>
              <span class="letters letters-right">NCH</span>
              <span class="line line2"></span>
            </span>
          </h1></a>
        `
    } else if (prog == -1) {
      return `<p class="loading-message ml3">${wrap_letter(m)}</p>
          <a href="javascript:quit_clicked();"><h1 class="ml5">
            <span class="text-wrapper">
                <span class="line line1"></span>
                <span class="letters letters-left">QU</span>
                <span class="letters letters-right">IT</span>
                <span class="line line2"></span>
            </span>
          </h1></a>
        `
    } else {
      return `<p class="loading-message ml3">${wrap_letter(m)}</p>
        <div class="sk-cube-grid">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
        </div>`
    }
}

function change_msg(msg, prog, est) {
    if (!loading_screen) {
        loading_screen = pleaseWait({
            // logo: logo,
            logo: 'make-m.png',
            backgroundColor: '#1d334b',
            // loadingHtml: "<p class='loading-message'>Preparing Installer</p>"
            // loadingHtml: "<div class='sk-spinner sk-spinner-wave'><div class='sk-rect1'></div><div class='sk-rect2'></div><div class='sk-rect3'></div><div class='sk-rect4'></div><div class='sk-rect5'></div></div>"
            // loadingHtml: `<p class="loading-message ml3">Getting a Java Runtime</p>
            loadingHtml: get_msg(msg, prog)
        });
    } else {
        function done() {
            return new Promise(function(resolve) {
                if (prog && !est) {
                    progress(prog, est);
                }
                animation.pause();
                loading_screen.updateLoadingHtml(get_msg(msg, prog), true);
                setTimeout(() => {
                    animation = anim();
                    resolve(animation.children[1].finished);
                }, 550);
            });
        }
        if (est) {
            progress(prog, est);
        }
        promise = promise.then(function() { return done(); });
    }
}

var loading_screen = undefined;

change_msg('Preparing installer');

var bar = new ProgressBar.Line('#bar', {
    strokeWidth: 4,
    easing: 'easeOut',
    duration: 1 * 1000,
    color: '#FFEA82',
    trailColor: '#eee',
    trailWidth: 1,
    svgStyle: {width: '100%', height: '100%'},
    from: {color: '#7ABA7A'},
    to: {color: '#028482'},
    step: (state, bar) => {
      bar.path.setAttribute('stroke', state.color);
    }
});

var animation = anim();
var promise = animation.children[1].finished;
window.launch_clicked = function() {
    change_msg("Launching");
    launch();
}
window.quit_clicked = function() {
    change_msg("Quitting");
    launch(1);
}