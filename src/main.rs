extern crate equoinstaller;

use std::thread::JoinHandle;
use std::time::Instant;

mod binaries;
fn main() {
    let cefswt_path = equoinstaller::cefswt_path();
    println!("cefswt: {:?}", cefswt_path);

    let now = Instant::now();

    let mut handles: Vec<JoinHandle<()>> = Vec::with_capacity(12);
    std::fs::create_dir_all(cefswt_path.as_path()).unwrap();
    let browser_p = binaries::expand(cefswt_path.as_path(), &mut handles); //extrae todos los bin en .cefswt y devuelve el path al .exe
                                                                           //NICO

    //let wpf_path =Path::new("C:\\Users\\nicol\\.swtcef\\3.3071.1649.g98725e6\\windows-x86_64\\wpfapps.exe");
    //let browser_p =
    //Path::new("C:\\Users\\nicol\\.swtcef\\3.3071.1649.g98725e6\\windows-x86_64\\wpfapps.exe");
    //  let browser_p = binaries::expand(wpf_path, &mut handles);
    println!("aaa1: {:?}", browser_p);
    println!("aaaa2: {:?}", handles);
    //NICO
    let html_path = std::env::temp_dir().join("equohtml");
    std::fs::create_dir_all(&html_path).expect("can't create html path");

    let index_html = binaries::expand_html(&html_path, &mut handles);
    for handle in handles {
        handle.join().unwrap();
    }
    println!(
        "extracted all in {:.5}s",
        now.elapsed().as_secs() as f64 + now.elapsed().subsec_nanos() as f64 * 1e-9
    );
    let args = std::env::args();
    let mut exe_name = Option::None;
    for (i, arg) in args.enumerate() {
        if arg == "--exeName" {
            exe_name = Option::Some(std::env::args().nth(i + 1).unwrap());
        }
    }
    let exe_name = match exe_name {
        Some(e) => e,
        None => {
            if cfg!(target_os = "macos") {
                std::env::current_exe()
                    .expect("Cannot get exe")
                    .parent()
                    .unwrap()
                    .parent()
                    .unwrap()
                    .parent()
                    .unwrap()
                    .file_stem()
                    .expect("Cannot get exe name")
                    .to_os_string()
                    .into_string()
                    .unwrap()
            } else {
                std::env::current_exe()
                    .expect("Cannot get executable")
                    .file_stem()
                    .expect("Cannot get executable name")
                    .to_os_string()
                    .into_string()
                    .unwrap()
            }
        }
    };

    println!("exeName: {}", exe_name);

    let mut child = if cfg!(target_os = "macos") {
        //let browser_p = browser_p.join("Contents/MacOS/cefrust_subp");
        let mut cmd = std::process::Command::new("open");
        let cmd = cmd
            .arg("-a")
            .arg(browser_p)
            .arg("-W")
            .arg("--args")
            .arg("--browser")
            .arg(format!("file://{}", index_html.to_str().unwrap()))
            .arg("--exeName")
            .arg(exe_name);
        println!("run: {:?}", cmd);
        cmd.spawn().expect("failed to execute browser")
    } else {
        let mut cmd = std::process::Command::new(browser_p);
        let cmd = cmd
            // .current_dir(cefswt_path)
            .arg("--browser")
            .arg(format!("file://{}", index_html.to_str().unwrap()))
            .arg("--exeName")
            .arg(exe_name);
        println!("run: {:?}", cmd);
        cmd.spawn().expect("failed to execute browser")
    };
    match child.try_wait() {
        Ok(Some(status)) => {
            println!("browser exited with: {}", status);
            std::process::exit(status.code().unwrap_or(0));
        }
        Ok(None) => {
            println!("status not ready yet, it's ok");
            if cfg!(not(target_os = "macos")) {
                let res = child.wait().unwrap();
                println!("result: {:?}", res);
                std::process::exit(res.code().unwrap_or(1));
            }
        }
        Err(e) => {
            println!("error attempting to wait: {}", e);
            std::process::exit(1);
        }
    }
}
