extern crate xz2;

use std::io::Write;
#[cfg(unix)]
use std::os::unix::fs::OpenOptionsExt;
use std::path::{Path, PathBuf};
use std::thread::{spawn, JoinHandle};
use std::time::Instant;

include!(concat!(env!("OUT_DIR"), "/binaries.rs"));
