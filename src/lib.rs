extern crate dirs;

use std::env;
use std::path::PathBuf;
// mod cefhandler;

pub const CEF_VERSION: &str = "3.3071.1649.g98725e6";

pub fn cefswt_path() -> PathBuf {
    match env::var("SWTCEF") {
        // It check if the env variable "swtcef" exists
        Ok(val) => PathBuf::from(val),
        Err(_err) => {
            let home = dirs::home_dir().unwrap_or(env::current_dir().unwrap());
            let os = match env::consts::OS {
                "macos" => "osx",
                s => s,
            };
            let cefswt_path = home.join(".swtcef").join(CEF_VERSION).join(format!(
                "{}-{}",
                os,
                env::consts::ARCH
            ));
            cefswt_path
        }
    }
}
