extern crate cefrust;
extern crate libc;

use self::cefrust::cef;
// use super::Msg;
use std::fs::{File, OpenOptions};
use std::io::Write;
use std::path::PathBuf;
use std::sync::mpsc;
use std::{mem, slice};

pub struct App {
    cef: cef::_cef_app_t,
    render_process_handler: RenderProcessHandler,
}

impl App {
    pub fn new() -> App {
        App {
            cef: App::cef_app(),
            render_process_handler: RenderProcessHandler::new(),
        }
    }

    pub fn as_ptr(&mut self) -> &mut cef::cef_app_t {
        &mut self.cef
    }

    fn cef_app() -> cef::cef_app_t {
        unsafe extern "C" fn get_render_process_handler(
            self_: *mut cef::_cef_app_t,
        ) -> *mut cef::_cef_render_process_handler_t {
            let a = self_ as *mut App;
            (*a).render_process_handler.as_ptr()
        }

        cef::cef_app_t {
            base: cefrust::base::CefBase::new(mem::size_of::<cef::cef_app_t>()),
            on_before_command_line_processing: Option::None,
            on_register_custom_schemes: Option::None,
            get_resource_bundle_handler: Option::None,
            get_browser_process_handler: Option::None,
            get_render_process_handler: Option::Some(get_render_process_handler),
        }
    }
}

#[derive(Debug)]
pub struct Client {
    cef: cef::_cef_client_t,
    load_handler: LoadHandler,
    pub app_name: Option<String>,
    pub install_folder: Option<PathBuf>,
    pub tx: Option<mpsc::Sender<Msg>>,
}

impl Client {
    pub fn new(
        on_process_message_received: unsafe extern "C" fn(
            *mut cef::_cef_client_t,
            *mut cef::_cef_browser_t,
            cef::cef_process_id_t,
            *mut cef::cef_process_message_t,
        ) -> libc::c_int,
        tx: mpsc::Sender<Msg>,
    ) -> Client {
        Client {
            cef: Client::cef_client(on_process_message_received),
            load_handler: LoadHandler::new(tx.clone()),
            app_name: Option::None,
            install_folder: Option::None,
            tx: Option::Some(tx),
        }
    }

    pub fn as_ptr(&mut self) -> *mut cef::_cef_client_t {
        &mut self.cef
    }

    fn cef_client(
        on_process_message_received: unsafe extern "C" fn(
            *mut cef::_cef_client_t,
            *mut cef::_cef_browser_t,
            cef::cef_process_id_t,
            *mut cef::cef_process_message_t,
        ) -> libc::c_int,
    ) -> cef::_cef_client_t {
        unsafe extern "C" fn get_load_handler(
            self_: *mut cef::_cef_client_t,
        ) -> *mut cef::_cef_load_handler_t {
            let c = self_ as *mut Client;
            (*c).load_handler.as_ptr()
        }

        cef::_cef_client_t {
            base: cefrust::base::CefBase::new(mem::size_of::<cef::_cef_client_t>()),
            get_context_menu_handler: Option::None,
            get_dialog_handler: Option::None,
            get_display_handler: Option::None,
            get_download_handler: Option::None,
            get_drag_handler: Option::None,
            get_find_handler: Option::None,
            get_focus_handler: Option::None,
            get_geolocation_handler: Option::None,
            get_jsdialog_handler: Option::None,
            get_keyboard_handler: Option::None,
            get_life_span_handler: Option::None,
            get_load_handler: Option::Some(get_load_handler),
            get_render_handler: Option::None,
            get_request_handler: Option::None,
            on_process_message_received: Option::Some(on_process_message_received),
        }
    }
}

#[derive(Debug)]
struct LoadHandler {
    cef: cef::_cef_load_handler_t,
    tx: mpsc::Sender<Msg>,
    loaded: bool,
}

#[derive(Clone, Copy)]
struct Browser(*mut cef::_cef_browser_t);
unsafe impl Send for Browser {}

impl LoadHandler {
    fn new(tx: mpsc::Sender<Msg>) -> LoadHandler {
        LoadHandler {
            cef: LoadHandler::cef_load_handler(),
            tx,
            loaded: false,
        }
    }

    pub fn as_ptr(&mut self) -> &mut cef::_cef_load_handler_t {
        &mut self.cef
    }

    fn on_load_end(&mut self) {
        if !self.loaded {
            self.loaded = true;

            self.tx
                .send(Msg {
                    step: Step::Loaded,
                    ..Default::default()
                })
                .expect("Failed to send");
        }
    }

    fn cef_load_handler() -> cef::_cef_load_handler_t {
        unsafe extern "C" fn on_load_end(
            this: *mut cef::_cef_load_handler_t,
            _browser: *mut cef::_cef_browser_t,
            frame: *mut cef::_cef_frame_t,
            _http_status_code: ::std::os::raw::c_int,
        ) {
            let lh = this as *mut LoadHandler;
            let self_ = &mut *lh;
            if (*frame).is_main.unwrap()(frame) == 1 {
                self_.on_load_end();
            }
        }

        cef::_cef_load_handler_t {
            base: cefrust::base::CefBase::new(mem::size_of::<cef::_cef_load_handler_t>()),
            on_loading_state_change: Option::None,
            on_load_start: Option::None,
            on_load_end: Option::Some(on_load_end),
            on_load_error: Option::None,
        }
    }
}

struct RenderProcessHandler {
    cef: cef::_cef_render_process_handler_t,
    launch_handler: Option<V8Handler>,
}

impl RenderProcessHandler {
    fn new() -> RenderProcessHandler {
        RenderProcessHandler {
            cef: RenderProcessHandler::cef_render_process_handler(),
            launch_handler: Option::None,
        }
    }

    pub fn as_ptr(&mut self) -> &mut cef::_cef_render_process_handler_t {
        &mut self.cef
    }

    fn cef_render_process_handler() -> cef::_cef_render_process_handler_t {
        unsafe extern "C" fn on_context_created(
            self_: *mut cef::_cef_render_process_handler_t,
            browser: *mut cef::_cef_browser_t,
            _frame: *mut cef::_cef_frame_t,
            context: *mut cef::_cef_v8context_t,
        ) {
            let rph = self_ as *mut RenderProcessHandler;

            let name = cefrust::cef_string("launch");
            (*rph).launch_handler = Option::Some(V8Handler::new(browser));
            let h: Option<&mut V8Handler> = (*rph).launch_handler.as_mut();
            let h: &mut V8Handler = h.expect("no handler");
            let func = cef::cef_v8value_create_function(&name, h.as_ptr());

            // Retrieve the context's window object.
            let get_global = (*context).get_global.expect("no global");
            let object = get_global(context);

            // Add the "myfunc" function to the "window" object.
            let set_value_bykey = (*object).set_value_bykey.expect("No set_value_bykey");
            let s = set_value_bykey(
                object,
                &name,
                func,
                cef::cef_v8_propertyattribute_t::V8_PROPERTY_ATTRIBUTE_NONE,
            );
            assert_eq!(s, 1);
        }

        cef::_cef_render_process_handler_t {
            base: cefrust::base::CefBase::new(mem::size_of::<cef::_cef_render_process_handler_t>()),
            on_render_thread_created: Option::None,
            on_web_kit_initialized: Option::None,
            on_browser_created: Option::None,
            on_browser_destroyed: Option::None,
            get_load_handler: Option::None,
            on_before_navigation: Option::None,
            on_context_created: Option::Some(on_context_created),
            on_context_released: Option::None,
            on_uncaught_exception: Option::None,
            on_focused_node_changed: Option::None,
            on_process_message_received: Option::None,
        }
    }
}

struct V8Handler {
    cef: cef::_cef_v8handler_t,
    browser: *mut cef::_cef_browser_t,
}

impl V8Handler {
    fn new(browser: *mut cef::_cef_browser_t) -> V8Handler {
        V8Handler {
            cef: V8Handler::cef_launch_handler(),
            browser: browser,
        }
    }

    pub fn as_ptr(&mut self) -> &mut cef::_cef_v8handler_t {
        &mut self.cef
    }

    fn cef_launch_handler() -> cef::_cef_v8handler_t {
        unsafe extern "C" fn execute(
            self_: *mut cef::_cef_v8handler_t,
            _name: *const cef::cef_string_t,
            _object: *mut cef::_cef_v8value_t,
            arguments_count: usize,
            _arguments: *const *const cef::_cef_v8value_t,
            _retval: *mut *mut cef::_cef_v8value_t,
            _exception: *mut cef::cef_string_t,
        ) -> libc::c_int {
            let handler = self_ as *mut V8Handler;
            let browser = (*handler).browser;

            let send_process_message = (*browser)
                .send_process_message
                .expect("no send_process_message");

            let name = if arguments_count == 1 {
                cefrust::cef_string("exit")
            } else {
                cefrust::cef_string("launch")
            };
            let msg = cef::cef_process_message_create(&name);
            let send = send_process_message(browser, cef::cef_process_id_t::PID_BROWSER, msg);
            assert_eq!(send, 1);
            1
        }

        cef::_cef_v8handler_t {
            base: cefrust::base::CefBase::new(mem::size_of::<cef::_cef_v8handler_t>()),
            execute: Option::Some(execute),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Step {
    None,
    Loaded,
    GotRunConfig,
    GotInstaller,
}

impl Default for Step {
    fn default() -> Step {
        Step::None
    }
}

#[derive(Default, Debug, Clone)]
pub struct Msg {
    pub msg: String,
    pub progress: f32,
    pub estimate: i32,
    pub app_name: String,
    pub install_folder: PathBuf,
    pub launch: bool,
    pub shutdown: bool,
    pub step: Step,
    pub data: PathBuf,
}

pub struct URLRequestClient {
    cef: cef::_cef_urlrequest_client_t,
    target: Option<PathBuf>,
    tx: mpsc::Sender<Msg>,
    progress: f32,
    step: Step,
}

impl URLRequestClient {
    pub fn new(step: Step, progress: f32, tx: mpsc::Sender<Msg>) -> URLRequestClient {
        URLRequestClient {
            cef: URLRequestClient::cef_urlrequest_client(),
            target: Option::None,
            step,
            progress,
            tx,
        }
    }

    fn cef_urlrequest_client() -> cef::_cef_urlrequest_client_t {
        unsafe extern "C" fn on_request_complete(
            this: *mut cef::_cef_urlrequest_client_t,
            request: *mut cef::_cef_urlrequest_t,
        ) {
            let urc = this as *mut URLRequestClient;
            let self_ = &*urc;
            let resp = (*request).get_response.unwrap()(request);
            let status = (*resp).get_status.unwrap()(resp);
            println!("on_request_complete: {:?} {}", self_.step, status);
            match (*request).get_request_status.unwrap()(request) {
                cef::cef_urlrequest_status_t::UR_SUCCESS if status < 400 => {
                    self_.done();
                }
                _s => {
                    self_
                        .tx
                        .send(Msg {
                            progress: -1.0,
                            step: self_.step,
                            msg: format!("{:?}", (*request).get_request_error.unwrap()(request)),
                            ..Default::default()
                        })
                        .unwrap();
                }
            }
        }
        unsafe extern "C" fn on_download_data(
            this: *mut cef::_cef_urlrequest_client_t,
            _request: *mut cef::_cef_urlrequest_t,
            data: *const ::std::os::raw::c_void,
            data_length: usize,
        ) {
            let urc = this as *mut URLRequestClient;
            let self_ = &*urc;
            // println!("on_download_data {:?}: {}", self_.step, data_length);
            match OpenOptions::new()
                .append(true)
                .open(self_.target.as_ref().expect("no target"))
            {
                Ok(mut file) => {
                    let bytes: &[u8] = slice::from_raw_parts(data as *const u8, data_length);
                    file.write_all(bytes)
                        .or_else(|_e| {
                            self_.tx.send(Msg {
                                progress: -1.0,
                                step: self_.step,
                                msg: format!(
                                    "Failed to write to {:?}",
                                    self_.target.as_ref().expect("no target")
                                ),
                                ..Default::default()
                            })
                        })
                        .unwrap();
                }
                Err(_e) => self_
                    .tx
                    .send(Msg {
                        progress: -1.0,
                        step: self_.step,
                        msg: format!(
                            "Filed to open {:?} for writting",
                            self_.target.as_ref().expect("no target")
                        ),
                        ..Default::default()
                    })
                    .unwrap(),
            };
        }

        cef::_cef_urlrequest_client_t {
            base: cefrust::base::CefBase::new(mem::size_of::<cef::_cef_urlrequest_client_t>()),
            on_request_complete: Option::Some(on_request_complete),
            on_upload_progress: Option::None,
            on_download_progress: Option::None,
            on_download_data: Option::Some(on_download_data),
            get_auth_credentials: Option::None,
        }
    }

    pub fn done(&self) {
        self.tx
            .send(Msg {
                progress: self.progress,
                step: self.step,
                data: self.target.as_ref().expect("no target").clone(),
                ..Default::default()
            })
            .unwrap();
    }

    pub fn done_with(&mut self, target: PathBuf) {
        self.target = Option::Some(target);
        self.done();
    }

    pub fn set_target(&mut self, target: PathBuf) {
        File::create(&target).unwrap();
        self.target = Option::Some(target);
    }

    pub fn as_ptr(&mut self) -> &mut cef::_cef_urlrequest_client_t {
        &mut self.cef
    }
}
