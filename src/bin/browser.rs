extern crate cefrust;
extern crate dirs;
extern crate equoinstaller;
extern crate xz2;

use cefrust::cef;
use cefrust::prepare_args;
use std::os::raw::c_int;
use std::path::{Path, PathBuf};

use app::Msg;
use app::Step;
use std::io::Write;
#[cfg(unix)]
use std::os::unix::fs::OpenOptionsExt;
use std::sync::mpsc;
use std::thread;
use std::time::Instant;

mod app;
mod cefhandler;

const INSTALLER: &str = "installer-0.1.3.jar";

include!(concat!(env!("OUT_DIR"), "/bin_launcher.rs"));

fn main() {
    let mut exe_name = Option::None;
    let mut html_path = Option::None;
    for (i, arg) in std::env::args().enumerate() {
        if arg == "--exeName" {
            exe_name = Option::Some(std::env::args().nth(i + 1).unwrap());
        } else if arg == "--browser" {
            html_path = Option::Some(std::env::args().nth(i + 1).unwrap());
        }
    }
    if exe_name.is_some() {
        println!("Starting browser");
        let exe_name = exe_name.expect("missing --exeName");
        let cefswt_path = equoinstaller::cefswt_path(); //devuelve el dir home/.... donde descomprimir
        let html_path = html_path.expect("missing --browser htmlpath");
        cef(&cefswt_path, &html_path, &exe_name);
    } else if std::env::args().len() <= 1 {
        // run_app();
    } else {
        subp();
    }
}

fn subp() {
    let main_args = prepare_args();
    println!("prepare args {:?}", main_args);
    println!("Calling cef_execute_process");
    let mut app = app::App::new();
    let exit_code: c_int =
        unsafe { cef::cef_execute_process(&main_args, app.as_ptr(), std::ptr::null_mut()) };
    println!("exiting subp with {}", exit_code);
    std::process::exit(exit_code);
}

fn cef(cefrust_path: &PathBuf, html_path: &str, exe_name: &str) {
    /* lo paso a cef_f
    let subp = cefrust::subp_path(cefrust_path); //termina siendo el browser.exe subp gets the path to cefrust_subp.exe
                                                 //let subp = "C:\\Users\\nicol\\.swtcef\\3.3071.1649.g98725e6\\windows-x86_64\\cefrust_subp.exe";
    println!("a ver si ahora:{:?}", subp); ///////BORRRRARRRRRRR
    let subp_cef = cefrust::cef_string(&subp);
    let resources_cef = if cfg!(target_os = "macos") {
        cefrust::cef_string(
            cefrust_path
                .join("Chromium Embedded Framework.framework")
                .join("Resources")
                .to_str()
                .unwrap(),
        )
    } else {
        cefrust::cef_string(cefrust_path.to_str().unwrap())
    };
    let locales_cef = if cfg!(target_os = "macos") {
        cefrust::cef_string(
            cefrust_path
                .join("Chromium Embedded Framework.framework")
                .join("Resources")
                .to_str()
                .unwrap(),
        )
    } else {
        cefrust::cef_string(cefrust_path.join("locales").to_str().unwrap())
    };
    // let locale = cefrust::cef_string("en-US");
    let locale = cefrust::cef_string_empty();
    let framework_dir_cef = if cfg!(target_os = "macos") {
        cefrust::cef_string(
            cefrust_path
                .join("Chromium Embedded Framework.framework")
                .to_str()
                .unwrap(),
        )
    } else {
        cefrust::cef_string_empty()
    };
    let cache_dir_cef = cefrust::cef_string(
        cefrust_path
            .parent()
            .unwrap()
            .parent()
            .unwrap()
            .join("cef_cache")
            .to_str()
            .unwrap(),
    );
    let logfile_cef = cefrust::cef_string(cefrust_path.join("lib.log").to_str().unwrap());

    let settings = cef::_cef_settings_t {
        size: std::mem::size_of::<cef::_cef_settings_t>(),
        single_process: 0,
        no_sandbox: 1,
        browser_subprocess_path: subp_cef,
        framework_dir_path: framework_dir_cef,
        multi_threaded_message_loop: 0,
        external_message_pump: 0,
        windowless_rendering_enabled: 0,
        command_line_args_disabled: 0,
        cache_path: cache_dir_cef,
        user_data_path: cefrust::cef_string_empty(),
        persist_session_cookies: 1,
        persist_user_preferences: 1,
        user_agent: cefrust::cef_string_empty(),
        product_version: cefrust::cef_string_empty(),
        locale: locale,
        log_file: logfile_cef,
        log_severity: cef::cef_log_severity_t::LOGSEVERITY_DEFAULT,
        javascript_flags: cefrust::cef_string_empty(),
        resources_dir_path: resources_cef,
        locales_dir_path: locales_cef,
        pack_loading_disabled: 0,
        remote_debugging_port: 9797,
        uncaught_exception_stack_size: 0,
        ignore_certificate_errors: 0,
        enable_net_security_expiration: 0,
        background_color: cef_color_set_argb(255, 0x1d, 0x33, 0x4b),
        accept_language_list: cefrust::cef_string_empty(),
    };
    let mut app = app::App::new(); //implementa todos los callbacks que cef requiere

    let main_args = cefrust::prepare_args();
    println!("Calling cef_initialize");
    unsafe { cef::cef_initialize(&main_args, &settings, app.as_ptr(), std::ptr::null_mut()) };
    HASTA ACA */
    cefhandler::init_cef(cefrust_path);
    //cambie por esto de arriba

    let window_info = cefhandler::cef_window_info();

    //cambie esto que era  let browser_settings = browser_settings(); y usaba browser_settings local
    let browser_settings = cefhandler::browser_settings(); // creo las settings del browser que voy a crear

    let (txmsg, rxmsg) = mpsc::channel(); // leeer! es un mecaniscmo de asincronismo de rust
    let (tx, rx) = mpsc::channel(); //threads independientes se comunican por msajes con estos channels

    let mut client = app::Client::new(cefhandler::on_process_message_received, tx.clone()); //obj que requiere cef con callbacks para el browser como  onclose oncreate

    let url = html_path;

    let url_cef = cefrust::cef_string(url);

    // Create browser.
    println!("Calling cef_browser_host_create_browser");
    let browser = unsafe {
        cef::cef_browser_host_create_browser_sync(
            &window_info,
            client.as_ptr(),
            &url_cef,
            &browser_settings,
            std::ptr::null_mut(),
        )
    };
    config_window(browser);

    let (app_name, channel, org_dash_app) = parse_exe(exe_name);
    tx.send(Msg {
        msg: format!("Getting '{}' configuration", app_name),
        progress: 0.07,
        estimate: 3,
        ..Default::default()
    })
    .unwrap(); //reporta progreso
    let mut config_req_client = app::URLRequestClient::new(Step::GotRunConfig, 0.07, tx.clone());
    get_app_config(&org_dash_app, &channel, &mut config_req_client); //obtengo el primer archivo que "bajo" que es el descriptor

    let mut installer_req_client = app::URLRequestClient::new(Step::GotInstaller, 0.20, tx.clone());

    println!("Calling cef_run_message_loop");

    let get_frame = unsafe { (*browser).get_main_frame.expect("null get_main_frame") };
    let main_frame = unsafe { get_frame(browser) };
    let execute = unsafe {
        (*main_frame)
            .execute_java_script
            .expect("null execute_java_script")
    };

    let mut loaded = false;
    // #[cfg(target_os = "macos")] let appfolder = format!("{}.app", &app_name);
    // #[cfg(not(target_os = "macos"))] let appfolder = &app_name;
    #[cfg(not(target_os = "macos"))]
    let install_folder = dirs::home_dir().unwrap().join(&app_name);
    #[cfg(target_os = "macos")]
    let install_folder = PathBuf::from("/Applications").join(format!("{}.app", &app_name));

    let mut app_config = PathBuf::new();

    loop {
        unsafe { cef::cef_do_message_loop_work() }; //ejecucion del event loop de cef, llama a cef y se ifja si hay mesnajes en el channel
        match rx.try_recv() {
            Ok(msg) => {
                println!("msg received {:?}", msg);
                let mut msg_clone = msg.clone();
                if msg.shutdown {
                    // shutdown();
                    std::process::exit(0);
                }
                if msg.launch {
                    client.app_name = Option::Some(msg.app_name);
                    client.install_folder = Option::Some(msg.install_folder);
                }

                if msg.progress == -1.0 {
                    match msg.step {
                        Step::GotRunConfig => {
                            msg_clone.msg = format!(
                                "Failed to get app '{} ({})', does the app exist?",
                                org_dash_app, channel
                            );
                        }
                        Step::GotInstaller => {
                            msg_clone.msg = format!(
                                "Failed to get '{}', shame on me or the network!",
                                INSTALLER
                            );
                        }
                        _ => {}
                    }
                    txmsg.send(msg_clone).expect("Failed to forward msg");
                } else {
                    txmsg.send(msg_clone).expect("Failed to forward msg");
                    match msg.step {
                        Step::Loaded => {
                            loaded = true;
                        }
                        Step::GotRunConfig => {
                            app_config.push(msg.data);

                            // let install_folder = ask_install_folder()

                            tx.send(Msg {
                                msg: format!("Getting '{}' files", app_name),
                                progress: 0.20,
                                estimate: 10,
                                ..Default::default()
                            })
                            .unwrap();
                            get_equo_installer(&cefrust_path, &mut installer_req_client);
                            //obtiene el jar para instalar la app equo
                        }
                        Step::GotInstaller => {
                            //instalacion propiamente dicha
                            let installer_jar = msg.data;

                            let msg = format!("Installing '{}'", app_name);
                            tx.send(Msg {
                                msg,
                                progress: 0.25,
                                estimate: 2,
                                ..Default::default()
                            })
                            .unwrap();

                            let app_name = app_name.to_owned();
                            let org_dash_app = org_dash_app.to_owned();
                            let channel = channel.to_owned();
                            let install_folder = install_folder.to_owned();
                            let app_config = app_config.to_owned();
                            let tx = tx.clone();
                            thread::spawn(move || {
                                match copy_launcher(&app_name, &install_folder, &app_config) {
                                    Ok(target_config) => {
                                        tx.send(Msg {
                                            progress: 0.25,
                                            ..Default::default()
                                        })
                                        .unwrap();
                                        println!("target config: {:?}", target_config);

                                        tx.send(Msg {
                                            progress: 1.0,
                                            estimate: 80,
                                            ..Default::default()
                                        })
                                        .unwrap();
                                        match run_equo_installer(
                                            &installer_jar,
                                            &org_dash_app,
                                            &channel,
                                            install_folder.to_str().unwrap(),
                                            target_config.to_str().unwrap(),
                                        ) {
                                            Ok(exit) if exit.success() => {
                                                // createLauncher(); // .desktop file
                                                let msg = format!("'{}' installed!", app_name);
                                                tx.send(Msg {
                                                    msg,
                                                    progress: 1.0,
                                                    app_name,
                                                    install_folder,
                                                    launch: true,
                                                    ..Default::default()
                                                })
                                                .unwrap();
                                            }
                                            Ok(exit) => {
                                                tx.send(Msg {
                                                    msg: format!(
                                                        "Error launching installer, exit code: {}",
                                                        exit.code().unwrap_or(-1)
                                                    ),
                                                    progress: -1.0,
                                                    app_name,
                                                    install_folder,
                                                    ..Default::default()
                                                })
                                                .unwrap();
                                            }
                                            Err(_e) => {
                                                tx.send(Msg {
                                                    msg: format!("Error launching installer"),
                                                    progress: -1.0,
                                                    app_name,
                                                    install_folder,
                                                    ..Default::default()
                                                })
                                                .unwrap();
                                            }
                                        }
                                    }
                                    Err(e) => {
                                        tx.send(Msg {
                                            progress: -1.0,
                                            app_name,
                                            install_folder,
                                            msg: format!("Failed to copy launcher: {:?}", e),
                                            ..Default::default()
                                        })
                                        .unwrap();
                                    }
                                }
                            });
                        }
                        Step::None => {}
                    }
                }
            }
            Err(..) => {}
        }
        if loaded {
            //si termino de cargar el html
            match rxmsg.try_recv() {
                Ok(msg) => {
                    if msg.step != Step::Loaded {
                        let est = if msg.estimate == 0 { 0 } else { msg.estimate };
                        if !msg.msg.is_empty() {
                            let msg1 =
                                format!("change_msg(\"{}\", {}, {});", msg.msg, msg.progress, est);
                            let code = cefrust::cef_string(&msg1);
                            unsafe { execute(main_frame, &code, &code, 0) }; // en el html hay una funcionchange message
                                                                             // println!("executed {}", msg1);
                        } else {
                            let msg1 = format!("progress({}, {});", msg.progress, est);
                            let code = cefrust::cef_string(&msg1);
                            unsafe { execute(main_frame, &code, &code, 0) };
                            // println!("executed {}", msg1);
                        }
                    }
                }
                Err(..) => {}
            }
        }
    }
}

fn parse_exe(exe_name: &str) -> (String, String, String) {
    let parts: Vec<&str> = exe_name
        .split_terminator(|c| c == '_' || c == '-')
        .collect();
    let org = parts[0];
    let app = parts[1];
    let channel = if parts.len() > 3 { parts[2] } else { "rls" };
    let org_dash_app = format!("{}-{}", org, app);
    (app.to_string(), channel.to_string(), org_dash_app)
}
/*
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Step {
    None,
    Loaded,
    GotRunConfig,
    GotInstaller
}

impl Default for Step {
    fn default() -> Step { Step::None }
}*/
/*
#[derive(Default, Debug, Clone)]
pub struct Msg {
    msg: String,
    progress: f32,
    estimate: i32,
    app_name: String,
    install_folder: PathBuf,
    launch: bool,
    shutdown: bool,
    step: Step,
    data: PathBuf
}
*/
fn copy_launcher(
    app_name: &str,
    install_folder: &PathBuf,
    app_config: &PathBuf,
) -> Result<PathBuf, std::io::Error> {
    println!("Copying launcher binary {:?} ", app_config); //copia el launcher .exe ene el dir de instalacion

    if cfg!(target_os = "macos") {
        let res = install_folder.join("Contents").join("Resources");
        std::fs::create_dir_all(&res)?;
        let cefswt_path = equoinstaller::cefswt_path();
        std::fs::copy(
            cefswt_path.join("cefrust_subp.app/Contents/Resources/icon.icns"),
            res.join(app_name).with_extension("icns"),
        )?;

        let mut plist = std::fs::File::create(install_folder.join("Contents").join("Info.plist"))?;
        write!(
            plist,
            r#"
        <?xml version="1.0" encoding="UTF-8"?>
        <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
        <plist version="1.0">
        <dict>
            <key>CFBundleDevelopmentRegion</key>
            <string>English</string>
            <key>CFBundleDisplayName</key>
            <string>{appname}</string>
            <key>CFBundleExecutable</key>
            <string>{appname}</string>
            <key>CFBundleGetInfoString</key>
            <string>{appname} for Mac OS.</string>
            <key>CFBundleIconFile</key>
            <string>{appname}.icns</string>
            <key>CFBundleIdentifier</key>
            <string>{appname}</string>
            <key>CFBundleInfoDictionaryVersion</key>
            <string>6.0</string>
            <key>CFBundleName</key>
            <string>{appname}</string>
            <key>CFBundlePackageType</key>
            <string>APPL</string>
            <key>CFBundleShortVersionString</key>
            <string>1.0.0</string>
            <key>CFBundleSignature</key>
            <string>????</string>
            <key>CFBundleVersion</key>
            <string>1.0.0</string>
        </dict>
        </plist>
        "#,
            appname = app_name
        )?;
    }

    #[cfg(target_os = "macos")]
    let install_folder = install_folder.join("Contents");
    // let mut config_f = std::fs::File::open(app_config).expect("Cannot open config file");
    let target_config = install_folder.join("launcher").with_extension("properties");
    // let mut target_config_f = std::fs::File::create(&target_config).expect("Cannot create config file");
    // std::io::copy(&mut config_f, &mut target_config_f).expect("Cannot create config file");
    std::fs::create_dir_all(&install_folder)?;
    std::fs::copy(app_config, &target_config)?;

    #[cfg(target_os = "macos")]
    let install_folder = install_folder.join("MacOS");
    #[cfg(target_os = "macos")]
    std::fs::create_dir_all(&install_folder)?;

    expand_launcher(install_folder.as_path(), app_name);

    Ok(target_config)
}
/* SE FUE A CEF_F
fn run_app(app_name: &str, install_folder: &Path, tx: mpsc::Sender<Msg>) {
    let install_folder = install_folder.to_owned();
    unsafe { cef::cef_do_message_loop_work() };
    let app_name = app_name.to_owned();
    std::thread::spawn(move || {
        #[cfg(target_os = "macos")]
        let mut child = std::process::Command::new("open")
            .current_dir(&install_folder)
            .arg("-a")
            .arg(install_folder)
            .arg("-W")
            .arg("--args")
            .arg("--firstRun")
            .spawn()
            .expect("failed to execute browser");
        #[cfg(not(target_os = "macos"))]
        let mut child = std::process::Command::new(install_folder.join(&app_name))
            .current_dir(install_folder)
            .arg("--firstRun")
            .spawn()
            .expect("failed to execute app");
        match child.try_wait() {
            Ok(Some(status)) => {
                println!("app exited with: {}", status);
                std::process::exit(status.code().unwrap_or(0));
            }
            Ok(None) => {
                println!("app launched");
                thread::sleep(std::time::Duration::from_secs(3));
                tx.send(Msg {
                    shutdown: true,
                    ..Default::default()
                })
                .unwrap();
            }
            Err(e) => {
                println!("error attempting to wait for app: {}", e);
                std::process::exit(1);
            }
        }
    });
}
*/

// fn shutdown() {
//      println!("Calling cef_shutdown");
//      // Shut down CEF.
//      unsafe { cef::cef_shutdown() };
// }

fn get_equo_url(comp: &str, channel: &str, path: &str) -> String {
    format!("https://dl.maketechnology.io/{}/{}/{}", comp, channel, path)
}

fn get_equo_installer(cefrust_path: &Path, req_client: &mut app::URLRequestClient) {
    let target_path = cefrust_path.join("installer.jar");
    // if !target_path.exists() {
    // TODO version the installer
    let local = PathBuf::from("installer.jar");
    if !target_path.exists() && !local.exists() {
        let url = get_equo_url("installer", "rls", INSTALLER);
        println!("Using installer {:?}", url);
        req_client.set_target(target_path.clone());
        let request = unsafe {
            let request = cefrust::cef::cef_request_create();
            (*request).set_url.unwrap()(request, &cefrust::cef_string(&url));
            (*request).set_method.unwrap()(request, &cefrust::cef_string("GET"));
            request
        };
        unsafe {
            cefrust::cef::cef_urlrequest_create(request, req_client.as_ptr(), std::ptr::null_mut())
        };
    } else if local.exists() {
        println!("Using installer {:?}", local);
        req_client.done_with(local.clone());
    } else if target_path.exists() {
        println!("Using installer {:?}", target_path);
        req_client.done_with(target_path.clone());
    }
    // }
}

fn get_app_config(org_dash_app: &str, channel: &str, req_client: &mut app::URLRequestClient) {
    #[cfg(target_os = "linux")]
    let os = "linux";
    #[cfg(windows)]
    let os = "win";
    #[cfg(target_os = "macos")]
    let os = "mac";
    let remote_bndrun = PathBuf::from(format!("launcher-{}", os)).with_extension("properties");
    if remote_bndrun.exists() {
        println!("Using config from {:?}", remote_bndrun);
        req_client.done_with(remote_bndrun);
    } else {
        let url = get_equo_url(org_dash_app, channel, remote_bndrun.to_str().unwrap());
        println!("Using config from {:?}", url);

        let target_path = std::env::temp_dir().join(remote_bndrun);
        req_client.set_target(target_path.clone()); //donde lo guardo
        let request = unsafe {
            let request = cefrust::cef::cef_request_create(); //aca lo descargo con cef
            (*request).set_url.unwrap()(request, &cefrust::cef_string(&url));
            (*request).set_method.unwrap()(request, &cefrust::cef_string("GET"));
            request
        };
        unsafe {
            cefrust::cef::cef_urlrequest_create(request, req_client.as_ptr(), std::ptr::null_mut())
        };
    }
}

fn run_equo_installer(
    installer_jar: &Path,
    org_dash_app: &str,
    channel: &str,
    install_folder: &str,
    bndrun_file: &str,
) -> Result<std::process::ExitStatus, std::io::Error> {
    let repo_url1 = get_equo_url("framework", "rls", "repo");
    let repo_url2 = get_equo_url(org_dash_app, channel, "repo");
    #[cfg(windows)]
    let java = "javaw.exe";
    #[cfg(not(windows))]
    let java = "java";
    #[cfg(target_os = "macos")]
    let install_folder = format!("{}/Contents", install_folder);
    let mut child = std::process::Command::new(java)
        // .current_dir(installer_jar.parent())
        .arg("-jar")
        .arg(installer_jar)
        .arg("-appName")
        .arg(org_dash_app)
        .arg("-install")
        .arg(install_folder)
        .arg("-bndrun")
        .arg(bndrun_file)
        .arg("-repoUrls")
        .arg(format!("{},{}", repo_url1, repo_url2))
        .spawn()?;
    child.wait()
}

const WIDTH: i32 = 620;
const HEIGHT: i32 = 420;

#[cfg(target_os = "linux")]
fn cef_window_info() -> cef::_cef_window_info_t {
    use std::os::raw::c_uint;

    // Create GTK window. You can pass a NULL handle
    // to CEF and then it will create a window of its own.
    //initialize_gtk();
    //let hwnd = create_gtk_window(String::from("cefcapi example"), 1024, 768);
    let window_info = cef::_cef_window_info_t {
        x: 0,
        y: 0,
        width: WIDTH as c_uint,
        height: HEIGHT as c_uint,
        parent_window: 0,
        windowless_rendering_enabled: 0,
        window: 0,
    };
    window_info
}

#[cfg(target_os = "macos")]
fn cef_window_info() -> cef::_cef_window_info_t {
    use std::os::raw::c_void;

    let window_info = cef::_cef_window_info_t {
        x: 0,
        y: 0,
        width: WIDTH,
        height: HEIGHT,
        parent_view: 0 as *mut c_void,
        windowless_rendering_enabled: 0,
        view: 0 as *mut c_void,
        hidden: 0,
        window_name: cefrust::cef_string("Equo Installer"),
    };
    window_info
}
/* se va a a cef_F
#[cfg(windows)]
fn cef_window_info() -> cef::_cef_window_info_t {
    extern crate winapi;

    let window_info = cef::_cef_window_info_t {
        x: winapi::um::winuser::CW_USEDEFAULT,
        y: winapi::um::winuser::CW_USEDEFAULT,
        width: WIDTH,
        height: HEIGHT,
        parent_window: std::ptr::null_mut() as cef::win::HWND,
        windowless_rendering_enabled: 0,
        window: 0 as cef::win::HWND,
        ex_style: 0,
        window_name: cefrust::cef_string("Equo Installer"),
        style: winapi::um::winuser::WS_POPUP
            | winapi::um::winuser::WS_CLIPCHILDREN
            | winapi::um::winuser::WS_CLIPSIBLINGS
            | winapi::um::winuser::WS_VISIBLE,
        menu: 0 as cef::win::HMENU,
    };
    window_info
} */
/*
fn browser_settings() -> cef::_cef_browser_settings_t {
    cef::_cef_browser_settings_t {
        size: std::mem::size_of::<cef::_cef_browser_settings_t>(),
        windowless_frame_rate: 0,
        standard_font_family: cefrust::cef_string_empty(),
        fixed_font_family: cefrust::cef_string_empty(),
        serif_font_family: cefrust::cef_string_empty(),
        sans_serif_font_family: cefrust::cef_string_empty(),
        cursive_font_family: cefrust::cef_string_empty(),
        fantasy_font_family: cefrust::cef_string_empty(),
        default_font_size: 0,
        default_fixed_font_size: 0,
        minimum_font_size: 0,
        minimum_logical_font_size: 0,
        default_encoding: cefrust::cef_string_empty(),
        remote_fonts: cef::cef_state_t::STATE_DEFAULT,
        javascript: cef::cef_state_t::STATE_DEFAULT,
        javascript_open_windows: cef::cef_state_t::STATE_DEFAULT,
        javascript_close_windows: cef::cef_state_t::STATE_DEFAULT,
        javascript_access_clipboard: cef::cef_state_t::STATE_DEFAULT,
        javascript_dom_paste: cef::cef_state_t::STATE_DEFAULT,
        plugins: cef::cef_state_t::STATE_DEFAULT,
        universal_access_from_file_urls: cef::cef_state_t::STATE_ENABLED,
        file_access_from_file_urls: cef::cef_state_t::STATE_ENABLED,
        web_security: cef::cef_state_t::STATE_DEFAULT,
        image_loading: cef::cef_state_t::STATE_DEFAULT,
        image_shrink_standalone_to_fit: cef::cef_state_t::STATE_DEFAULT,
        text_area_resize: cef::cef_state_t::STATE_DEFAULT,
        tab_to_links: cef::cef_state_t::STATE_DEFAULT,
        local_storage: cef::cef_state_t::STATE_DEFAULT,
        databases: cef::cef_state_t::STATE_DEFAULT,
        application_cache: cef::cef_state_t::STATE_DEFAULT,
        webgl: cef::cef_state_t::STATE_DEFAULT,
        background_color: 0,
        accept_language_list: cefrust::cef_string_empty(),
    }
}
*/
#[cfg(target_os = "linux")]
extern crate x11;

#[cfg(target_os = "linux")]
fn config_window(browser: *mut cef::_cef_browser_t) {
    use x11::xlib;

    #[allow(unused)]
    struct Data {
        flags: u32,
        functions: u32,
        decorations: u32,
        input_mode: i32,
        status: u32,
    }
    let data = Data {
        flags: 2,
        functions: 0,
        decorations: 0,
        input_mode: 0,
        status: 0,
    };
    let xdisplay = unsafe { std::mem::transmute(cef::cef_get_xdisplay()) };
    let browser_host = cef_f::get_browser_host(browser);
    let get_window_handle_fn = unsafe {
        (*browser_host)
            .get_window_handle
            .expect("no get_window_handle")
    };
    let xwindow = unsafe { get_window_handle_fn(browser_host) };

    let x_type = std::ffi::CStr::from_bytes_with_nul(b"_MOTIF_WM_HINTS\0").unwrap();
    let atom = unsafe { xlib::XInternAtom(xdisplay, x_type.as_ptr(), xlib::True) };
    unsafe {
        xlib::XChangeProperty(
            xdisplay,
            xwindow,
            atom,
            atom,
            32,
            xlib::PropModeReplace,
            std::mem::transmute(&data),
            5,
        )
    };

    let screen = unsafe { xlib::XDefaultScreen(xdisplay) };
    let width = unsafe { xlib::XDisplayWidth(xdisplay, screen) };
    let height = unsafe { xlib::XDisplayHeight(xdisplay, screen) };
    unsafe {
        xlib::XMoveWindow(
            xdisplay,
            xwindow,
            (width - WIDTH) / 2,
            (height - HEIGHT) / 2,
        )
    };
}

#[cfg(target_os = "windows")]
fn config_window(browser: *mut cef::_cef_browser_t) {
    extern crate winapi;

    let browser_host = cefhandler::get_browser_host(browser);
    let get_window_handle_fn = unsafe {
        (*browser_host)
            .get_window_handle
            .expect("no get_window_handle")
    };
    let hwnd = unsafe { get_window_handle_fn(browser_host) } as winapi::shared::windef::HWND;

    let parent_window = unsafe { winapi::um::winuser::GetDesktopWindow() };
    let mut rect = winapi::shared::windef::RECT {
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
    };
    unsafe { winapi::um::winuser::GetClientRect(parent_window, &mut rect) };
    rect.left = (rect.right / 2) - (WIDTH / 2);
    rect.top = (rect.bottom / 2) - (HEIGHT / 2);
    unsafe { winapi::um::winuser::MoveWindow(hwnd, rect.left, rect.top, WIDTH, HEIGHT, 0) };
}

#[cfg(target_os = "macos")]
extern crate cocoa;

#[cfg(target_os = "macos")]
fn config_window(browser: *mut cef::_cef_browser_t) {
    use cocoa::appkit::{
        NSApplicationActivateIgnoringOtherApps, NSEvent, NSRunningApplication, NSWindow,
        NSWindowStyleMask, NSWindowTitleVisibility,
    };
    use cocoa::base::{id, nil};
    use cocoa::foundation::{NSPoint, NSString};

    let browser_host = cef_f::get_browser_host(browser);
    let get_window_handle_fn = unsafe {
        (*browser_host)
            .get_window_handle
            .expect("no get_window_handle")
    };
    let view = unsafe { get_window_handle_fn(browser_host) } as id;

    unsafe {
        let win = view.window();
        win.setTitle_(NSString::alloc(nil).init_str("Equo Installer"));
        win.setTitleVisibility_(NSWindowTitleVisibility::NSWindowTitleHidden);
        win.setStyleMask_(NSWindowStyleMask::NSBorderlessWindowMask);
        win.cascadeTopLeftFromPoint_(NSPoint::new(20., 20.));
        win.center();
        win.makeKeyAndOrderFront_(nil);

        let app = NSRunningApplication::currentApplication(nil);
        app.activateWithOptions_(NSApplicationActivateIgnoringOtherApps);
    }
}
/* se fue a cef_f
fn get_browser_host(browser: *mut cef::cef_browser_t) -> *mut cef::_cef_browser_host_t {
    let get_host_fn = unsafe { (*browser).get_host.expect("null get_host") };
    let browser_host = unsafe { get_host_fn(browser) };
    browser_host
}

unsafe extern "C" fn on_process_message_received(
    cefclient: *mut cef::_cef_client_t,
    _cefbrowser: *mut cef::_cef_browser_t,
    source_process: cef::cef_process_id_t,
    message: *mut cef::cef_process_message_t,
) -> ::std::os::raw::c_int {
    if source_process == cef::cef_process_id_t::PID_RENDERER {
        let valid = (*message).is_valid.unwrap()(message);
        let name = (*message).get_name.unwrap()(message);

        let launch = cefrust::cef_string("launch");
        let exit = cefrust::cef_string("exit");
        if valid == 1 && cef::cef_string_utf16_cmp(&launch, name) == 0 {
            let client = cefclient as *mut app::Client;
            // let b = cefbrowser as *mut app::Browser;
            let app_name: &str = (*client).app_name.as_ref().expect("No appname");
            let install_folder: &Path = (*client)
                .install_folder
                .as_ref()
                .expect("No install folder");
            let tx = (*client).tx.as_ref().expect("no sender");
            run_app(app_name, install_folder, tx.clone());
        } else if valid == 1 && cef::cef_string_utf16_cmp(&exit, name) == 0 {
            std::process::exit(0);
        }

        cef::cef_string_userfree_utf16_free(name);
        return 1;
    }
    0
}

fn cef_color_set_argb(a: u32, r: u32, g: u32, b: u32) -> cef::cef_color_t {
    (a << 24) | (r << 16) | (g << 8) | (b << 0)
}
*/
