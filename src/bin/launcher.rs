extern crate equoinstaller;

fn main() {
    let cefswt_path = equoinstaller::cefswt_path();
    println!("cefswt: {:?}", cefswt_path);

    // if !cefswt_path.exists() {
    //     panic!("{:?} does not exist", &cefswt_path);
    // }

    let exe = std::env::current_exe().unwrap();
    let app_p = exe.parent().unwrap();
    println!("app_p: {:?}", app_p);
    #[cfg(target_os = "macos")]
    let app_p = app_p.parent().unwrap();
    let equo_p = app_p.join(".equo");
    let sep = if cfg!(windows) { ";" } else { ":" };
    let classpath = format!(
        "{}{}{}",
        app_p.to_str().unwrap(),
        sep,
        equo_p.to_str().unwrap()
    );

    #[cfg(windows)]
    let java = "javaw.exe";
    #[cfg(unix)]
    let java = "java";
    let mut c = std::process::Command::new(java);
    let cmd = c.current_dir(app_p);
    let cmd = if cfg!(target_os = "macos") {
        cmd.arg("-XstartOnFirstThread")
    } else if cfg!(target_os = "linux") {
        cmd.env("SWT_GTK3", "0")
    } else {
        cmd
    };
    let mut child = cmd
        .arg("-cp")
        .arg(classpath)
        .arg("com.make.equo.launcher.Launcher")
        .arg("--proxy-server=localhost:9896")
        .arg("--ignore-certificate-errors")
        .arg("--allow-file-access-from-files")
        .arg("--disable-web-security")
        .arg("--enable-widevine-cdm")
        .arg("--proxy-bypass-list=127.0.0.1")
        .spawn()
        .expect("failed to execute java from launcher");
    match child.try_wait() {
        Ok(Some(status)) => {
            println!("java exited with: {}", status);
            std::process::exit(status.code().unwrap_or(0));
        }
        Ok(None) => {
            let res = child.wait().unwrap();
            println!("result: {:?}", res);
            std::process::exit(res.code().unwrap_or(1));
        }
        Err(e) => {
            println!("java attempting to wait: {}", e);
            std::process::exit(1);
        }
    }
}
