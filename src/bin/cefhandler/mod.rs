use super::app;
use std::path::{Path, PathBuf};
extern crate directories;
extern crate dirs;
extern crate fs_extra;
use app::Msg;
use cefrust::cef;
use std::sync::mpsc;
use std::thread;


pub fn cef_color_set_argb(a: u32, r: u32, g: u32, b: u32) -> cefrust::cef::cef_color_t {
    (a << 24) | (r << 16) | (g << 8) | (b << 0)
}

pub fn init_cef(cefrust_path: &PathBuf) {
    println!("cefrust_path init cef{:?}", cefrust_path);
    let subp = cefrust::subp_path(cefrust_path);
    let subp_cef = cefrust::cef_string(&subp);
    let framework_dir_cef = if cfg!(target_os = "macos") {
        cefrust::cef_string(
            cefrust_path
                .join("Chromium Embedded Framework.framework")
                .to_str()
                .unwrap(),
        )
    } else {
        cefrust::cef_string_empty()
    };
    let resources_cef = if cfg!(target_os = "macos") {
        cefrust::cef_string(
            cefrust_path
                .join("Chromium Embedded Framework.framework")
                .join("Resources")
                .to_str()
                .unwrap(),
        )
    } else {
        println!("cefrust_path.to_str {:?}", cefrust_path.to_str());
        cefrust::cef_string(cefrust_path.to_str().unwrap())
    };
    let locales_cef = if cfg!(target_os = "macos") {
        cefrust::cef_string(
            cefrust_path
                .join("Chromium Embedded Framework.framework")
                .join("Resources")
                .to_str()
                .unwrap(),
        )
    } else {
        cefrust::cef_string(cefrust_path.join("locales").to_str().unwrap())
    };
    let cache_dir_cef = cefrust::cef_string(
        cefrust_path
            .parent()
            .unwrap()
            .parent()
            .unwrap()
            .join("cef_cache")
            .to_str()
            .unwrap(),
    );
    let locale = cefrust::cef_string_empty();
    let logfile_cef = cefrust::cef_string(cefrust_path.join("lib.log").to_str().unwrap());
    let settings = cefrust::cef::_cef_settings_t {
        size: std::mem::size_of::<cefrust::cef::_cef_settings_t>(),
        single_process: 0,
        no_sandbox: 1,
        browser_subprocess_path: subp_cef, //lauch subprocces for cef
        framework_dir_path: framework_dir_cef,
        multi_threaded_message_loop: 0,
        external_message_pump: 1, // lo cambie segun la documentacion para usar cef_do_message_loop_work()
        windowless_rendering_enabled: 0,
        command_line_args_disabled: 0,
        cache_path: cache_dir_cef,
        user_data_path: cefrust::cef_string_empty(),
        persist_session_cookies: 1,
        persist_user_preferences: 1,
        user_agent: cefrust::cef_string_empty(),
        product_version: cefrust::cef_string_empty(),
        locale: locale,
        log_file: logfile_cef,
        log_severity: cefrust::cef::cef_log_severity_t::LOGSEVERITY_DEFAULT,
        javascript_flags: cefrust::cef_string_empty(),
        resources_dir_path: resources_cef,
        locales_dir_path: locales_cef,
        pack_loading_disabled: 0,
        remote_debugging_port: 9797,
        uncaught_exception_stack_size: 0,
        ignore_certificate_errors: 0,
        enable_net_security_expiration: 0,
        background_color: cef_color_set_argb(255, 0x1d, 0x33, 0x4b),
        accept_language_list: cefrust::cef_string_empty(),
    };
    let mut app = app::App::new(); //implementa todos los callbacks que cef requiere
    let main_args = cefrust::prepare_args();
    println!("Calling cef_initialize");
    unsafe {
        cefrust::cef::cef_initialize(&main_args, &settings, app.as_ptr(), std::ptr::null_mut())
    };
}

pub fn browser_settings() -> cef::_cef_browser_settings_t {
    cef::_cef_browser_settings_t {
        size: std::mem::size_of::<cef::_cef_browser_settings_t>(),
        windowless_frame_rate: 0,
        standard_font_family: cefrust::cef_string_empty(),
        fixed_font_family: cefrust::cef_string_empty(),
        serif_font_family: cefrust::cef_string_empty(),
        sans_serif_font_family: cefrust::cef_string_empty(),
        cursive_font_family: cefrust::cef_string_empty(),
        fantasy_font_family: cefrust::cef_string_empty(),
        default_font_size: 0,
        default_fixed_font_size: 0,
        minimum_font_size: 0,
        minimum_logical_font_size: 0,
        default_encoding: cefrust::cef_string_empty(),
        remote_fonts: cef::cef_state_t::STATE_DEFAULT,
        javascript: cef::cef_state_t::STATE_DEFAULT,
        javascript_open_windows: cef::cef_state_t::STATE_DEFAULT,
        javascript_close_windows: cef::cef_state_t::STATE_DEFAULT,
        javascript_access_clipboard: cef::cef_state_t::STATE_DEFAULT,
        javascript_dom_paste: cef::cef_state_t::STATE_DEFAULT,
        plugins: cef::cef_state_t::STATE_DEFAULT,
        universal_access_from_file_urls: cef::cef_state_t::STATE_ENABLED,
        file_access_from_file_urls: cef::cef_state_t::STATE_ENABLED,
        web_security: cef::cef_state_t::STATE_DEFAULT,
        image_loading: cef::cef_state_t::STATE_DEFAULT,
        image_shrink_standalone_to_fit: cef::cef_state_t::STATE_DEFAULT,
        text_area_resize: cef::cef_state_t::STATE_DEFAULT,
        tab_to_links: cef::cef_state_t::STATE_DEFAULT,
        local_storage: cef::cef_state_t::STATE_DEFAULT,
        databases: cef::cef_state_t::STATE_DEFAULT,
        application_cache: cef::cef_state_t::STATE_DEFAULT,
        webgl: cef::cef_state_t::STATE_DEFAULT,
        background_color: 0,
        accept_language_list: cefrust::cef_string_empty(),
    }
}

const WIDTH: i32 = 620;
const HEIGHT: i32 = 420;
#[cfg(windows)]
pub fn cef_window_info() -> cef::_cef_window_info_t {
    extern crate winapi;

    let window_info = cef::_cef_window_info_t {
        x: winapi::um::winuser::CW_USEDEFAULT,
        y: winapi::um::winuser::CW_USEDEFAULT,
        width: WIDTH,
        height: HEIGHT,
        parent_window: std::ptr::null_mut() as cef::win::HWND,
        windowless_rendering_enabled: 0,
        window: 0 as cef::win::HWND,
        ex_style: 0,
        window_name: cefrust::cef_string("Equo Installer"),
        style: winapi::um::winuser::WS_POPUP
            | winapi::um::winuser::WS_CLIPCHILDREN
            | winapi::um::winuser::WS_CLIPSIBLINGS
            | winapi::um::winuser::WS_VISIBLE,
        menu: 0 as cef::win::HMENU,
    };
    window_info
}

pub fn get_browser_host(browser: *mut cef::cef_browser_t) -> *mut cef::_cef_browser_host_t {
    let get_host_fn = unsafe { (*browser).get_host.expect("null get_host") };
    let browser_host = unsafe { get_host_fn(browser) };
    browser_host
}

pub unsafe extern "C" fn on_process_message_received(
    cefclient: *mut cef::_cef_client_t,
    _cefbrowser: *mut cef::_cef_browser_t,
    source_process: cef::cef_process_id_t,
    message: *mut cef::cef_process_message_t,
) -> ::std::os::raw::c_int {
    if source_process == cef::cef_process_id_t::PID_RENDERER {
        let valid = (*message).is_valid.unwrap()(message);
        let name = (*message).get_name.unwrap()(message);

        let launch = cefrust::cef_string("launch");
        let exit = cefrust::cef_string("exit");
        if valid == 1 && cef::cef_string_utf16_cmp(&launch, name) == 0 {
            let client = cefclient as *mut app::Client;
            // let b = cefbrowser as *mut app::Browser;
            let app_name: &str = (*client).app_name.as_ref().expect("No appname");
            let install_folder: &Path = (*client)
                .install_folder
                .as_ref()
                .expect("No install folder");
            let tx = (*client).tx.as_ref().expect("no sender");
            run_app(app_name, install_folder, tx.clone());
        } else if valid == 1 && cef::cef_string_utf16_cmp(&exit, name) == 0 {
            std::process::exit(0);
        }

        cef::cef_string_userfree_utf16_free(name);
        return 1;
    }
    0
}

fn run_app(app_name: &str, install_folder: &Path, tx: mpsc::Sender<Msg>) {
    let install_folder = install_folder.to_owned();
    unsafe { cef::cef_do_message_loop_work() };
    let app_name = app_name.to_owned();
    std::thread::spawn(move || {
        #[cfg(target_os = "macos")]
        let mut child = std::process::Command::new("open")
            .current_dir(&install_folder)
            .arg("-a")
            .arg(install_folder)
            .arg("-W")
            .arg("--args")
            .arg("--firstRun")
            .spawn()
            .expect("failed to execute browser");
        #[cfg(not(target_os = "macos"))]
        let mut child = std::process::Command::new(install_folder.join(&app_name))
            .current_dir(install_folder)
            .arg("--firstRun")
            .spawn()
            .expect("failed to execute app");
        match child.try_wait() {
            Ok(Some(status)) => {
                println!("app exited with: {}", status);
                std::process::exit(status.code().unwrap_or(0));
            }
            Ok(None) => {
                println!("app launched");
                thread::sleep(std::time::Duration::from_secs(3));
                tx.send(Msg {
                    shutdown: true,
                    ..Default::default()
                })
                .unwrap();
            }
            Err(e) => {
                println!("error attempting to wait for app: {}", e);
                std::process::exit(1);
            }
        }
    });
}
