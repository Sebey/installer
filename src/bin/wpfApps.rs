use std::env;
use std::fs;
use std::path::{Path, PathBuf};
extern crate dirs;
use directories::UserDirs;
extern crate directories;
extern crate fs_extra;
use app::Msg;
use app::Step;
use cefrust::cef;
use std::sync::mpsc;

mod app;
mod cefhandler;
fn main() {
    let destination = get_home_dir().unwrap().join("installer"); //cambiar por el nombre de la app
                                                                 //let cefrust_path = get_current_dir().join("target\\release");
    let app_name = String::from("FirstApp");
    let probando = PathBuf::from("C:\\Users\\nicol\\.swtcef\\3.3071.1649.g98725e6\\windows-x86_64");
    cefhandler::init_cef(&probando /*&cefrust_path*/);
    init_browser(
        "C:\\Users\\nicol\\OneDrive\\Desktop\\launcher\\html\\dist\\index.html",
        &app_name,
        &destination,
    );
    // get_wpf_installer(&destination, "FirstApp", "nico");
    //  create_unistaller_file(&destination);
    //open_installer(&destiny);  TO RUN THE APP INSTANTLY AFTER RUN THE INSTALER
}

fn get_home_dir() -> Option<PathBuf> {
    dirs::home_dir() //for windows
}
fn get_current_dir() -> PathBuf {
    let path = env::current_dir().unwrap(); //for windows
    path
}

fn create_unistaller_file(destination: &PathBuf) {
    let dir = env::current_dir()
        .unwrap()
        .join("target\\release\\equoinstaller.exe");
    println!("asd{:?}", dir);

    match fs::File::create(&destination.join("unistaller.exe")) {
        Ok(_) => println!("unistaller was created!"),
        Err(err) => println!("{:?}", err),
    }
    match fs::copy(&dir, &destination.join("unistaller.exe")) {
        Ok(_) => println!("unistaller was copied!"),
        Err(err) => println!("{:?}", err),
    }
}

fn create_app_dir(destiny: &PathBuf) {
    match fs::create_dir(destiny) {
        Ok(_) => println!("folder was created!"),
        Err(err) => println!("{:?}", err),
    }
}

/*
fn open_installer(destiny: &Path) {
    let file = &destiny.join("Debug\\FirstApp.exe");
    let path: String = file.to_str().unwrap().to_string();
    let output = Command::new(path).output();
    println!("{:?}", output);
}
*/
fn create_desktop_instaler(destiny: &Path) {
    // get desktop path
    let user_dirs = directories::UserDirs::new();
    let nwp = &user_dirs.unwrap();
    let desktop_path = UserDirs::desktop_dir(nwp);
    // ............... //
    let file = &destiny.join("FirstApp.exe");
    println!("filefilefile{:?}", &file);
    let path: PathBuf = desktop_path.unwrap().join("FirstApp.exe");
    match fs::File::create(&path) {
        Ok(_) => println!(".exe was created!"),
        Err(err) => println!("{:?}", err),
    }
    match fs::copy(&file, &path) {
        Ok(_) => println!(".exe was copied!"),
        Err(err) => println!("{:?}", err),
    }
}

// HERE  ITS ALL I NEED TO DOWNLOAD THE FILES FROM DIGITAL OCEAN
fn get_wpf_url(comp: &str, channel: &str, path: &String) -> String {
    // Cambiar el repo por fra1
    format!(
        "https://equo-{}.nyc3.digitaloceanspaces.com/{}/{}",
        comp, channel, path
    )
}

fn append_exe_to_file(nombre: &str) -> String {
    //rename fn append_.exe
    let mut nom = String::from(nombre);
    let exe = String::from(".exe");
    nom.push_str(&exe);
    nom
}

//ACA VOY A PONER TODO LO NECESARIO PARA LEVANTAR LA UI/////////
fn init_browser(html_path: &str, application_name: &String, cefrust_path: &PathBuf) {
    println!("Calling html{:?}", html_path);
    println!("Calling app {:?}", application_name);
    println!("Calling cefr{:?}", cefrust_path);
    let (tx, rx) = mpsc::channel();
    tx.send(Msg {
        msg: format!("Getting '{}' configuration", application_name),
        progress: 3.0,
        estimate: 3,
        ..Default::default()
    })
    .unwrap();
    let mut client = app::Client::new(cefhandler::on_process_message_received, tx.clone()); //obj que requiere cef con callbacks para el browser como  onclose oncreate
    let mut installer_req_client = app::URLRequestClient::new(Step::GotInstaller, 2.0, tx.clone());
    let window_info = cefhandler::cef_window_info();
    let browser_settings = cefhandler::browser_settings();
    let url = html_path;
    let url_cef = cefrust::cef_string(url);
    println!("Calling cef_browser_host_create_browser");
    let browser = unsafe {
        cef::cef_browser_host_create_browser_sync(
            &window_info,
            client.as_ptr(),
            &url_cef,
            &browser_settings,
            std::ptr::null_mut(),
        )
    };
    config_window(browser);
    let mut loaded = false;
    let get_frame = unsafe { (*browser).get_main_frame.expect("null get_main_frame") };
    let main_frame = unsafe { get_frame(browser) };
    let execute = unsafe {
        (*main_frame)
            .execute_java_script
            .expect("null execute_java_script")
    };

    loop {
        unsafe { cefrust::cef::cef_do_message_loop_work() };
        match rx.try_recv() {
            Ok(msg) => {
                println!("msg received: {:?}", msg);
                if msg.shutdown {
                    // shutdown();
                    std::process::exit(0);
                }
                if msg.progress == -1.0 {
                    println! {"entre -1 "};
                    match msg.step {
                        Step::GotRunConfig => {
                            println! {"entre Run Config "};
                        }
                        Step::GotInstaller => {
                            println! {"entre Installer"};
                        }
                        Step::None => {
                            println! {"entre None"};
                        }
                        Step::Loaded => {
                            println! {"entre Loaded3"};
                        }
                    }
                } else {
                    match msg.step {
                        Step::GotRunConfig => {
                            println! {"entre RunConfig"};
                        }
                        Step::GotInstaller => {
                            println! {"entre Installer "};
                            //get_wpf_installer(&cefrust_path, "FirstApp", "nico",  &mut installer_req_client);
                        }
                        Step::None => {
                            println! {"entre None adsd"};
                        }
                        Step::Loaded => {
                            println! {"entre loaded4 "};
                            loaded = true;
                            let app_name = append_exe_to_file(application_name);
                            let install_folder = PathBuf::from(cefrust_path);
                            let message = get_wpf_installer("FirstApp", "nico", &mut installer_req_client);
                            if message == "Installed" {
                                let msg = format!("Installed");
                                tx.send(Msg {
                                    msg,
                                    progress: 2.0,
                                    app_name,
                                    install_folder,
                                    estimate: 2,
                                    launch: true,
                                    ..Default::default()
                                })
                                .unwrap();
                                create_unistaller_file(&cefrust_path);
                            } else {
                                let msg = format!("Uninstalled");
                                tx.send(Msg {
                                    msg,
                                    progress: 2.0,
                                    app_name,
                                    estimate: 2,
                                    ..Default::default()
                                })
                                .unwrap();
                            }
                        }
                    }
                }
            }
            Err(..) => { /* TO DO sleep cpu */ }
        }
        if loaded {
            match rx.try_recv() {
                Ok(msg) => {
                    if msg.msg == "Installed" {
                        //println!("msg received: {:?}", msg);
                        create_desktop_instaler(&msg.install_folder);
                        //client.app_name = Option::Some(msg.app_name);
                        // client.install_folder = Option::Some(msg.install_folder);
                        // let est = if msg.estimate == 0 { 0 } else { msg.estimate };
                        // let msg1 = format!("change_msg(\"{}\", {}, {});", msg.msg, msg.progress, est);
                        // let code = cefrust::cef_string(&msg1);
                        // unsafe { execute(main_frame, &code, &code, 0) };
                    }
                    println!("msg received: {:?}", msg);
                    client.app_name = Option::Some(msg.app_name);
                    client.install_folder = Option::Some(msg.install_folder);
                    let est = if msg.estimate == 0 { 0 } else { msg.estimate };
                    let msg1 = format!("change_msg(\"{}\", {}, {});", msg.msg, msg.progress, est);
                    let code = cefrust::cef_string(&msg1);
                    unsafe { execute(main_frame, &code, &code, 0) };
                }
                Err(..) => {}
            }
        }
    }
}
///ACA TERMINA LO QUE VOY A PONER PARA LEVANTAR LA UI/////////

fn get_wpf_installer(
    app_name: &str,
    space: &str,
    req_client: &mut app::URLRequestClient,
) -> String {
    let mut value = String::from("not action done");
    let cefrust_path = get_home_dir().unwrap().join("installer");
    //Este exists deberia ser sobre el .exe por si el dir existe pero tiene otras cosas
    if cefrust_path.exists() {
        value = unistaller(&cefrust_path, app_name);
    // ACA DEBERIA TIRAR UN WARING PARA VER QUE QUIERE HACER PORQUE YA EXISTE EL FOLDER
    } else {
        create_app_dir(&cefrust_path);
        //}
        let exe_file = append_exe_to_file(app_name);
        let target_path = cefrust_path.join(&exe_file);
        // TODO version the installer
        let local = PathBuf::from(&exe_file);
        if !target_path.exists() && !local.exists() {
            // todo esto hace un request, puede ir en un metodo a cef
            let url = get_wpf_url(space, "rls", &exe_file);
            println!("Using installer {:?}", url);
            req_client.set_target(target_path.clone());
            let request = unsafe {
                let request = cefrust::cef::cef_request_create();
                (*request).set_url.unwrap()(request, &cefrust::cef_string(&url));
                (*request).set_method.unwrap()(request, &cefrust::cef_string("GET"));
                request
            };
            println!("request {:?}", request);
            unsafe {
                cefrust::cef::cef_urlrequest_create(
                    request,
                    req_client.as_ptr(),
                    std::ptr::null_mut(),
                )
            }; //todo esto hace un request, puede ir en un metodo a cef
            unsafe { cefrust::cef::cef_do_message_loop_work() };
            value = String::from("Installed");
        }
    }
    value
}

fn unistaller(dir: &PathBuf, app_name: &str) -> String {
    let app_exe = append_exe_to_file(app_name);
    println!("Do you want to delete or repair the app?{:?}", dir);
    let mut value = String::from("default");
    let user_dirs = directories::UserDirs::new();
    let result = fs::remove_dir_all(&dir);
    let nwp = &user_dirs.unwrap();
    //app_name = append_exe_to_file(app_name);
    let desktop_path: PathBuf = UserDirs::desktop_dir(nwp).unwrap().join(app_exe);
    if fs::remove_file(&desktop_path).is_ok() {
        println!("the desktop access has been removed");
    }
    if result.is_ok() {
        println!("the app has been unistalled");
        value = String::from("unistalled");
    }
    value
}
////ESTO SE VA A IR A UN NUEVO FILE DE CONFIGURACIONES DE WINDOWS
fn config_window(browser: *mut cef::_cef_browser_t) {
    extern crate winapi;

    let browser_host = cefhandler::get_browser_host(browser);
    let get_window_handle_fn = unsafe {
        (*browser_host)
            .get_window_handle
            .expect("no get_window_handle")
    };
    let hwnd = unsafe { get_window_handle_fn(browser_host) } as winapi::shared::windef::HWND;

    let parent_window = unsafe { winapi::um::winuser::GetDesktopWindow() };
    let mut rect = winapi::shared::windef::RECT {
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
    };
    unsafe { winapi::um::winuser::GetClientRect(parent_window, &mut rect) };
    rect.left = (rect.right / 2) - (WIDTH / 2);
    rect.top = (rect.bottom / 2) - (HEIGHT / 2);
    unsafe { winapi::um::winuser::MoveWindow(hwnd, rect.left, rect.top, WIDTH, HEIGHT, 0) };
}

///ESTO SE VA A IR A AL NUEVO FILE DE CEF
const WIDTH: i32 = 620;
const HEIGHT: i32 = 420;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn this_test_will_pass() {
        assert_eq!(4, 2 + 2);
    }
    /*
        #[test]
        fn this_test_will_fail() {
            println!("failure");
            assert_eq!(5, 2+2);
        }
    */
    #[test]
    fn get_wpf_installer_test() {
        let cefrust_path = get_current_dir().join("\\target\\release");
        let destination = get_home_dir().unwrap().join("installer-test");
        cefhandler::init_cef(&cefrust_path);
        let (tx, rx) = mpsc::channel();
        tx.send(Msg {
            msg: format!("Getting '{}' configuration", "test app"),
            progress: 3.0,
            estimate: 3,
            ..Default::default()
        })
        .unwrap();
        let mut installer_req_client =
            app::URLRequestClient::new(Step::GotInstaller, 0.20, tx.clone());
        get_wpf_installer("test", "installer-test", &mut installer_req_client);
        let x = true;
        assert!(x, destination.join("test.exe").exists());
    }

    /*  #[test]
    fn uninstall_test() {
        let dir = get_home_dir().unwrap().join("unistaller");
        match fs::create_dir_all(&dir) {
            Ok(_) => println!("folder was created!"),
            Err(err) => println!("{:?}", err),
        }
        let file  = "C:\\Users\\nicol\\OneDrive\\Desktop\\file.exe";
        match fs::File::create(&dir.join("file.exe")) {
            Ok(_) => println!("file was created!"),
            Err(err) => println!("{:?}", err),
        }
        unistaller(&dir,&file);
        let x = false;
        println!("hola{:?}", dir.exists());
        assert_eq!(x, dir.exists());
    }*/
}
